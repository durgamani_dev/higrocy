package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the tbl_order_master database table.
 * 
 */
@Entity
@Table(name="tbl_order_master")
@NamedQuery(name="TblOrderMaster.findAll", query="SELECT t FROM TblOrderMaster t")
public class TblOrderMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ORDER_ID")
	private String orderId;

	@Column(name="ORDER_KEY")
	private String orderKey;

	@Column(name="PRODUCT_ID")
	private BigInteger productId;

	private int quantity;

	@Column(name="RATING_ID")
	private int ratingId;

	public TblOrderMaster() {
	}

	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderKey() {
		return this.orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

	public BigInteger getProductId() {
		return this.productId;
	}

	public void setProductId(BigInteger productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getRatingId() {
		return this.ratingId;
	}

	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}

}