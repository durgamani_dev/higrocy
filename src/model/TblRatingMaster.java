package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_rating_master database table.
 * 
 */
@Entity
@Table(name="tbl_rating_master")
@NamedQuery(name="TblRatingMaster.findAll", query="SELECT t FROM TblRatingMaster t")
public class TblRatingMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RATING_ID")
	private int ratingId;

	private String rating;

	public TblRatingMaster() {
	}

	public int getRatingId() {
		return this.ratingId;
	}

	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}

	public String getRating() {
		return this.rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

}