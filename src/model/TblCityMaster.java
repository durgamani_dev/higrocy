package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_city_master database table.
 * 
 */
@Entity
@Table(name="tbl_city_master")
@NamedQuery(name="TblCityMaster.findAll", query="SELECT t FROM TblCityMaster t")
public class TblCityMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CITY_ID")
	private int cityId;

	@Lob
	private String description;

	private String name;

	@Column(name="STATE_ID")
	private int stateId;

	private int status;

	public TblCityMaster() {
	}

	public int getCityId() {
		return this.cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}