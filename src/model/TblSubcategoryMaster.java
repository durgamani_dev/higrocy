package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_subcategory_master database table.
 * 
 */
@Entity
@Table(name="tbl_subcategory_master")
@NamedQuery(name="TblSubcategoryMaster.findAll", query="SELECT t FROM TblSubcategoryMaster t")
public class TblSubcategoryMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUBCATEGORY_ID")
	private int subcategoryId;

	@Column(name="CATEGORY_ID")
	private int categoryId;

	private String subcategory;

	public TblSubcategoryMaster() {
	}

	public int getSubcategoryId() {
		return this.subcategoryId;
	}

	public void setSubcategoryId(int subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getSubcategory() {
		return this.subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

}