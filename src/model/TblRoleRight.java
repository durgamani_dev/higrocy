package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_role_right database table.
 * 
 */
@Entity
@Table(name="tbl_role_right")
@NamedQuery(name="TblRoleRight.findAll", query="SELECT t FROM TblRoleRight t")
public class TblRoleRight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ROLE_RIGHT_ID")
	private int roleRightId;

	@Column(name="RIGHT_ID")
	private int rightId;

	@Column(name="ROLE_ID")
	private int roleId;

	public TblRoleRight() {
	}

	public int getRoleRightId() {
		return this.roleRightId;
	}

	public void setRoleRightId(int roleRightId) {
		this.roleRightId = roleRightId;
	}

	public int getRightId() {
		return this.rightId;
	}

	public void setRightId(int rightId) {
		this.rightId = rightId;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

}