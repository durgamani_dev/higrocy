package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the tbl_product_master database table.
 * 
 */
@Entity
@Table(name="tbl_product_master")
@NamedQuery(name="TblProductMaster.findAll", query="SELECT t FROM TblProductMaster t")
public class TblProductMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PRODUCT_ID")
	private String productId;

	@Column(name="BRAND_ID")
	private int brandId;

	@Column(name="CATEGORY_ID")
	private int categoryId;

	@Column(name="CREATED_BY")
	private BigInteger createdBy;

	@Column(name="CREATED_ON")
	private byte[] createdOn;

	private String description;

	private String image;

	@Column(name="PRODUCT_NAME")
	private String productName;

	@Column(name="PRODUCT_NAME_HIN")
	private String productNameHin;

	@Column(name="PRODUCT_NAME_KAN")
	private String productNameKan;

	@Column(name="PRODUCT_NAME_MAL")
	private String productNameMal;

	@Column(name="PRODUCT_NAME_ORI")
	private String productNameOri;

	@Column(name="PRODUCT_NAME_TAM")
	private String productNameTam;

	@Column(name="PRODUCT_NAME_TEL")
	private String productNameTel;

	@Column(name="PRODUCT_TYPE_ID")
	private int productTypeId;

	private int status;

	@Column(name="UPDATED_BY")
	private BigInteger updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_ON")
	private Date updatedOn;

	public TblProductMaster() {
	}

	public String getProductId() {
		return this.productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public int getBrandId() {
		return this.brandId;
	}

	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public BigInteger getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigInteger createdBy) {
		this.createdBy = createdBy;
	}

	public byte[] getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(byte[] createdOn) {
		this.createdOn = createdOn;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductNameHin() {
		return this.productNameHin;
	}

	public void setProductNameHin(String productNameHin) {
		this.productNameHin = productNameHin;
	}

	public String getProductNameKan() {
		return this.productNameKan;
	}

	public void setProductNameKan(String productNameKan) {
		this.productNameKan = productNameKan;
	}

	public String getProductNameMal() {
		return this.productNameMal;
	}

	public void setProductNameMal(String productNameMal) {
		this.productNameMal = productNameMal;
	}

	public String getProductNameOri() {
		return this.productNameOri;
	}

	public void setProductNameOri(String productNameOri) {
		this.productNameOri = productNameOri;
	}

	public String getProductNameTam() {
		return this.productNameTam;
	}

	public void setProductNameTam(String productNameTam) {
		this.productNameTam = productNameTam;
	}

	public String getProductNameTel() {
		return this.productNameTel;
	}

	public void setProductNameTel(String productNameTel) {
		this.productNameTel = productNameTel;
	}

	public int getProductTypeId() {
		return this.productTypeId;
	}

	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public BigInteger getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(BigInteger updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}