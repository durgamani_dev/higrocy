package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the tbl_faq database table.
 * 
 */
@Entity
@Table(name="tbl_faq")
@NamedQuery(name="TblFaq.findAll", query="SELECT t FROM TblFaq t")
public class TblFaq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="FAQ_ID")
	private String faqId;

	@Lob
	private String answer;

	@Lob
	private String description;

	@Column(name="PRODUCT_ID")
	private BigInteger productId;

	@Lob
	private String question;

	private String status;

	@Column(name="USER_ID")
	private BigInteger userId;

	public TblFaq() {
	}

	public String getFaqId() {
		return this.faqId;
	}

	public void setFaqId(String faqId) {
		this.faqId = faqId;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigInteger getProductId() {
		return this.productId;
	}

	public void setProductId(BigInteger productId) {
		this.productId = productId;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}