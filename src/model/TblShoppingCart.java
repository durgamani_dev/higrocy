package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the tbl_shopping_cart database table.
 * 
 */
@Entity
@Table(name="tbl_shopping_cart")
@NamedQuery(name="TblShoppingCart.findAll", query="SELECT t FROM TblShoppingCart t")
public class TblShoppingCart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SHOPPING_CART_ID")
	private String shoppingCartId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON")
	private Date createdOn;

	@Column(name="ORDER_ID")
	private BigInteger orderId;

	@Column(name="SESSION_ID")
	private String sessionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_ON")
	private Date updatedOn;

	@Column(name="USER_ID")
	private BigInteger userId;

	public TblShoppingCart() {
	}

	public String getShoppingCartId() {
		return this.shoppingCartId;
	}

	public void setShoppingCartId(String shoppingCartId) {
		this.shoppingCartId = shoppingCartId;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public BigInteger getOrderId() {
		return this.orderId;
	}

	public void setOrderId(BigInteger orderId) {
		this.orderId = orderId;
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}