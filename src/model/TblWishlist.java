package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the tbl_wishlist database table.
 * 
 */
@Entity
@Table(name="tbl_wishlist")
@NamedQuery(name="TblWishlist.findAll", query="SELECT t FROM TblWishlist t")
public class TblWishlist implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="WISHLIST_ID")
	private String wishlistId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON")
	private Date createdOn;

	@Column(name="PRODUCT_ID")
	private BigInteger productId;

	@Column(name="USER_ID")
	private BigInteger userId;

	public TblWishlist() {
	}

	public String getWishlistId() {
		return this.wishlistId;
	}

	public void setWishlistId(String wishlistId) {
		this.wishlistId = wishlistId;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public BigInteger getProductId() {
		return this.productId;
	}

	public void setProductId(BigInteger productId) {
		this.productId = productId;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}