package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the tbl_order_discount database table.
 * 
 */
@Entity
@Table(name="tbl_order_discount")
@NamedQuery(name="TblOrderDiscount.findAll", query="SELECT t FROM TblOrderDiscount t")
public class TblOrderDiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TBL_ORDER_DISCOUNT")
	private String tblOrderDiscount;

	@Column(name="DISCOUNT_ID")
	private int discountId;

	@Column(name="ORDER_ID")
	private BigInteger orderId;

	public TblOrderDiscount() {
	}

	public String getTblOrderDiscount() {
		return this.tblOrderDiscount;
	}

	public void setTblOrderDiscount(String tblOrderDiscount) {
		this.tblOrderDiscount = tblOrderDiscount;
	}

	public int getDiscountId() {
		return this.discountId;
	}

	public void setDiscountId(int discountId) {
		this.discountId = discountId;
	}

	public BigInteger getOrderId() {
		return this.orderId;
	}

	public void setOrderId(BigInteger orderId) {
		this.orderId = orderId;
	}

}