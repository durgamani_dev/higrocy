package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_role_master database table.
 * 
 */
@Entity
@Table(name="tbl_role_master")
@NamedQuery(name="TblRoleMaster.findAll", query="SELECT t FROM TblRoleMaster t")
public class TblRoleMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ROLE_ID")
	private int roleId;

	@Lob
	private String description;

	private int hirerichy;

	@Column(name="ROLE_NAME")
	private String roleName;

	public TblRoleMaster() {
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getHirerichy() {
		return this.hirerichy;
	}

	public void setHirerichy(int hirerichy) {
		this.hirerichy = hirerichy;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}