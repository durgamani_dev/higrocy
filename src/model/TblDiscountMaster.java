package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_discount_master database table.
 * 
 */
@Entity
@Table(name="tbl_discount_master")
@NamedQuery(name="TblDiscountMaster.findAll", query="SELECT t FROM TblDiscountMaster t")
public class TblDiscountMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	private String discount;

	@Column(name="DISCOUNT_ID")
	private int discountId;

	public TblDiscountMaster() {
	}

	public String getDiscount() {
		return this.discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public int getDiscountId() {
		return this.discountId;
	}

	public void setDiscountId(int discountId) {
		this.discountId = discountId;
	}

}