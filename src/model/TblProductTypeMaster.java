package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_product_type_master database table.
 * 
 */
@Entity
@Table(name="tbl_product_type_master")
@NamedQuery(name="TblProductTypeMaster.findAll", query="SELECT t FROM TblProductTypeMaster t")
public class TblProductTypeMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PRODUCT_TYPE_ID")
	private int productTypeId;

	@Column(name="PRODUCT_DESCRIPTION")
	private String productDescription;

	@Column(name="PRODUCT_TYPE")
	private String productType;

	public TblProductTypeMaster() {
	}

	public int getProductTypeId() {
		return this.productTypeId;
	}

	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}

	public String getProductDescription() {
		return this.productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

}