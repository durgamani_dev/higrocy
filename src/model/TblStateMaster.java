package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_state_master database table.
 * 
 */
@Entity
@Table(name="tbl_state_master")
@NamedQuery(name="TblStateMaster.findAll", query="SELECT t FROM TblStateMaster t")
public class TblStateMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STATE_ID")
	private int stateId;

	@Lob
	private String description;

	private String name;

	private int status;

	public TblStateMaster() {
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}