package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_right_master database table.
 * 
 */
@Entity
@Table(name="tbl_right_master")
@NamedQuery(name="TblRightMaster.findAll", query="SELECT t FROM TblRightMaster t")
public class TblRightMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="RIGHT_ID")
	private int rightId;

	private String description;

	@Column(name="RIGHT_NAME")
	private byte[] rightName;

	public TblRightMaster() {
	}

	public int getRightId() {
		return this.rightId;
	}

	public void setRightId(int rightId) {
		this.rightId = rightId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getRightName() {
		return this.rightName;
	}

	public void setRightName(byte[] rightName) {
		this.rightName = rightName;
	}

}