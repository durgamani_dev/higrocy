package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_category_master database table.
 * 
 */
@Entity
@Table(name="tbl_category_master")
@NamedQuery(name="TblCategoryMaster.findAll", query="SELECT t FROM TblCategoryMaster t")
public class TblCategoryMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CATEGORY_ID")
	private int categoryId;

	private String category;

	public TblCategoryMaster() {
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}