package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the tbl_user_account database table.
 * 
 */
@Entity
@Table(name="tbl_user_account")
@NamedQuery(name="TblUserAccount.findAll", query="SELECT t FROM TblUserAccount t")
public class TblUserAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USER_ACCOUNT_ID")
	private String userAccountId;

	@Lob
	private String answer;

	@Lob
	private String description;

	@Lob
	@Column(name="HINT_QUESTION")
	private String hintQuestion;

	private String password;

	@Column(name="ROLE_ID")
	private int roleId;

	@Column(name="UPDATED_BY")
	private BigInteger updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_ON")
	private Date updatedOn;

	@Column(name="USER_ID")
	private BigInteger userId;

	@Column(name="USER_NAME")
	private String userName;

	public TblUserAccount() {
	}

	public String getUserAccountId() {
		return this.userAccountId;
	}

	public void setUserAccountId(String userAccountId) {
		this.userAccountId = userAccountId;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHintQuestion() {
		return this.hintQuestion;
	}

	public void setHintQuestion(String hintQuestion) {
		this.hintQuestion = hintQuestion;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public BigInteger getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(BigInteger updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}